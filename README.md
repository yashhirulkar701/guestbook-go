## Guestbook Go application CI-CD

This example shows how to build a CI-CD pipeine for a simple multi-tier web application using `Gitlab CI` and `Kubernetes Cluster`. The application consists of a web front end, Redis master for storage, and replicated set of Redis replicas, all for which we will create Kubernetes replication controllers, pods, and services using CI-CD pipeline.

### Pipeline Stages:-
```yaml
stages:
    - test
    - build 
    - deploy
```

#### Stage 1: Static application security testing (SAST)

Static Application Security Testing (SAST) checks source code to find possible security vulnerabilities. SAST helps developers identify weaknesses and security issues earlier in the software development lifecycle before code is deployed. SAST usually is performed when code is being submitted to a code repository. Think of it like spell check for security issues.
```yaml
include:
  - template: Jobs/SAST.gitlab-ci.yml
sast:
    stage: test
```

#### Stage 2: Container Image build and push

In this stage we will be building container image for guestbook application and pushing it to the gitlab `container registry`.
```yaml
image_build:
    stage: build
    image: docker:20.10.16
    services:
        - docker:20.10.16-dind
    variables:
        DOCKER_TLS_CERTDIR: "/certs"
    before_script:
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    script:
        - docker build -t $IMAGE_NAME:$CI_PIPELINE_IID .
    after_script:
        - docker push $IMAGE_NAME:$CI_PIPELINE_IID
```

#### Stage 3: Deployment on kubernetes cluster 

In this stage we will be deploying the guestbook application on kubernetes cluster using the manifest files present in `kubernetes` folder. This stage will require manual approval.
```yaml
deploy:
  stage: deploy
  tags:
    - shell
  before_script:
    - export KUBECONFIG=$KUBE_CONFIG
    - export NAMESPACE=$NAME_SPACE
    - export IMAGE_NAME=$IMAGE_NAME
    - export IMAGE_TAG=$CI_PIPELINE_IID
  script: 
    - kubectl create secret docker-registry gitlab-docker-registry --docker-server=$CI_REGISTRY   --docker-username=$GITLAB_USER --docker-password=$GITLAB_PASSWORD -n guestbook --dry-run=client -o yaml | kubectl apply -f -
    - envsubst <  kubernetes/redis-master-controller.yaml | kubectl apply -f -
    - envsubst <  kubernetes/redis-master-service.yaml | kubectl apply -f -
    - envsubst <  kubernetes/redis-replica-controller.yaml | kubectl apply -f -
    - envsubst <  kubernetes/redis-replica-service.yaml | kubectl apply -f -
    - envsubst <  kubernetes/guestbook-controller.yaml | kubectl apply -f -
    - envsubst <  kubernetes/guestbook-service.yaml | kubectl apply -f -
  when: manual
```
